# For plotting
import matplotlib as mpl
mpl.use("Agg")
from matplotlib import pyplot as plt

# For reading csv file
import pandas

from optparse import OptionParser

# Parsing args to get the csv filename
parser = OptionParser()
(options, filename) = parser.parse_args()

muon_data = pandas.read_csv(filename[0])

azimuth_angles = muon_data.columns.values[1:].astype(int)

plt.figure()
plt.grid()
plt.title("Angular dependence of cosmic muon concentration")
plt.xlabel("Azimuth angle")
plt.ylabel("Count")

for index in muon_data.index:
	count_list = muon_data.iloc[index][1:]
	plt.plot(azimuth_angles, count_list.as_matrix(), marker = "o", label = "Altitude = {0} degree".format(muon_data.iloc[:,0][index]))

plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.0)
plt.savefig("scatter_plot.png", bbox_inches='tight')
